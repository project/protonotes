<?php
// $Id$

/**
 * @file
 * Administration page callbacks for the protonotes module.
 */

/**
 * Form builder. Configure protonotes.
 *
 * @ingroup forms
 * @see system_settings_form().
 */
function protonotes_admin_settings() {
  // Get an array of node types with internal names as keys and
  // "friendly names" as values. E.g.,
  // array('page' => 'Page', 'story' => 'Story')
  $options = node_get_types('names');
  
  $form['protonotes_enabled'] = array( 
    '#type' => 'radio', 
    '#title' => t('Do you want to enable protonotes?'), 
    '#options' => $options, 
    '#default_value' => variable_get('protonotes_enabled', array('page')), 
    '#description' => t('A site wide toggle for using protonotes.'), 
  ); 

  $form['protonotes_node_types'] = array( 
    '#type' => 'checkboxes', 
    '#title' => t('Users may protonotes these content types'), 
    '#options' => $options, 
    '#default_value' => variable_get('protonotes_node_types', array('page')), 
    '#description' => t('A text field will be available on these content types to make user-specific notes.'), 
  ); 

  $form['protonotes_node_types'] = array( 
    '#type' => 'checkboxes', 
    '#title' => t('Users may protonotes these content types'), 
    '#options' => $options, 
    '#default_value' => variable_get('protonotes_node_types', array('page')), 
    '#description' => t('A text field will be available on these content types to make user-specific notes.'), 
  ); 
  return system_settings_form($form); 

}